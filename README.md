# NHL API Trading Advisor
---
To build, run these commands :
> npm install && npm run tsc

Then, to serve the website:
> npm run web

or directly: 
> node server.js