import express from 'express';
import path from 'path';

const app = express();
app.use(express.static('public'))
app.get('/', async function(req, res) {
    res.sendFile('public/index.html', {
        root: path.join(__dirname, './')
    })
})
app.listen(8080, '0.0.0.0', () => console.log('Example app listening on port 8080!'));

