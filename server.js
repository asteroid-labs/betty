"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const app = express_1.default();
app.use(express_1.default.static('public'));
app.get('/', async function (req, res) {
    res.sendFile('public/index.html', {
        root: path_1.default.join(__dirname, './')
    });
});
app.listen(8080, '0.0.0.0', () => console.log('Example app listening on port 8080!'));
