let teams = {}
let season = 20212022
let data = []
async function main(){
    let res = await fetch(`https://statsapi.web.nhl.com/api/v1/standings/?season=${season}`)
    teams = await res.json()
    for (const division of teams.records) {
        for (const team of division.teamRecords) {
            res = await fetch(`https://statsapi.web.nhl.com/api/v1/teams/${team.team.id}/stats?season=${season}`)
            let stats = await res.json()
            let goalsPerGame = stats.stats[0].splits[0].stat.goalsPerGame
            // let gag = stats.stats[0].splits[0].stat.goalshotsAllowedainstPerGame
            let shotsPerGame = stats.stats[0].splits[0].stat.shotsPerGame
            let shotsAllowed = stats.stats[0].splits[0].stat.shotsAllowed
            data.push({
                "name": team.team.name,
                "rank": Number(team.leagueRank),
                // "GP": team.gamesPlayed,
                // "G": team.goalsScored,
                // "GA": team.goalshotsAllowedainst,
                "goalsPerGame": Number(goalsPerGame),
                // "GAG": gag,
                // "GDIFF": Number((goalsPerGame - gag).toFixed(3)),
                "shotsPerGame": Number(shotsPerGame),
                "shotsAllowed": Number(shotsAllowed),
                //"SDIFF": Number((shotsPerGame - shotsAllowed).toFixed(3)),

          /*       "gamesPlayed" : 6,
                "wins" : 4,
                "losses" : 0,
                "ot" : 2,
                "pts" : 10,
                "ptPctg" : "83.3",
                "goalsPerGame" : 4.333,
                "goalsAgainstPerGame" : 2.667,
                "evGGARatio" : 2.8571,
                "powerPlayPercentage" : "14.3",
                "powerPlayGoals" : 3.0,
                "powerPlayGoalsAgainst" : 5.0,
                "powerPlayOpportunities" : 21.0,
                "penaltyKillPercentage" : "70.6",
                "shotsPerGame" : 30.5,
                "shotsAllowed" : 27.5,
                "winScoreFirst" : 0.75,
                "winOppScoreFirst" : 0.5,
                "winLeadFirstPer" : 1.0,
                "winLeadSecondPer" : 0.8,
                "winOutshootOpp" : 0.75,
                "winOutshotByOpp" : 0.5,
                "faceOffsTaken" : 343.0,
                "faceOffsWon" : 162.0,
                "faceOffsLost" : 181.0,
                "faceOffWinPercentage" : "47.2",
                "shootingPctg" : 14.2,
                "savePctg" : 0.903 */
            })
            data.sort(function (a, b) {
                if(a.name > b.name)
                return 1
                if(a.name < b.name)
                return -1
                return 0
              });
        }
    }
    let select = document.createElement('select')
    let select2 = document.createElement('select')
    select.setAttribute('id', 'sel1')
    select2.setAttribute('id', 'sel2')

    for (let i = 0; i < data.length; i++) {
        let option = document.createElement('option');
        option.setAttribute('value', i);
        option.appendChild(document.createTextNode(data[i].name));
        select.appendChild(option);
        
        let option2 = document.createElement('option');
        option2.setAttribute('value', i);
        option2.appendChild(document.createTextNode(data[i].name));
        select2.appendChild(option2);
    }

    document.getElementById('select1').appendChild(select)
    document.getElementById('select2').appendChild(select2)

    let url = new URL(window.location.href);
    let selected1 = url.searchParams.get("sel1");
    let selected2 = url.searchParams.get("sel2");
    if(selected1 && selected2){
        document.getElementById('name1').innerHTML = data[selected1].name
        document.getElementById('name2').innerHTML = data[selected2].name
        document.getElementById('shots1').innerHTML = ('('+data[selected1].shotsPerGame.toFixed(2)+' / '+ data[selected1].shotsAllowed.toFixed(2)+')')
        document.getElementById('shots2').innerHTML = ('('+data[selected2].shotsPerGame.toFixed(2)+' / '+ data[selected2].shotsAllowed.toFixed(2)+')')

        // ratio de shots_equipe1 vs shotsAllowed_equipe2 = rendement de l'equipe1 vs equipe2
        let tirs1 = Number(data[selected1].shotsPerGame * 100 / data[selected2].shotsAllowed).toFixed(2) // 30.5/27.4 = 111.31
        let tirs2 = Number(data[selected2].shotsPerGame * 100 / data[selected1].shotsAllowed).toFixed(2) // 31.4/27.5 = 114.18

        // potentiel de tir aux buts, % de chance que l'equipe fasse plus de buts que l'autre
        let pot_tirs1 = (tirs1-100).toFixed(2) //Number(((Number(tirs1) + Number(data[selected2].shotsAllowed * 100 / data[selected1].shotsPerGame)) / 2 ) - 100).toFixed(2) // ((111.31 + (27.4*100/30.5))/2) - 100
        document.getElementById('pot_tirs1').innerHTML = pot_tirs1 + ' %'
        let pot_tirs2 = (tirs2-100).toFixed(2) //Number(((Number(tirs2) + Number(data[selected1].shotsAllowed * 100 / data[selected2].shotsPerGame)) / 2 ) - 100).toFixed(2)
        document.getElementById('pot_tirs2').innerHTML = pot_tirs2 + ' %'
        if(Number(pot_tirs1) > Number(pot_tirs2)){
            //green
            document.getElementById("pot_tirs1").className = "table-success";
        }else {
            document.getElementById("pot_tirs2").className = "table-success";
        }

/*         //% +tirs total
        let tir_tot1 = (Number(Number(tirs1) + Number(tirs2))/2).toFixed(2)
        document.getElementById('tir_tot1').innerHTML = tir_tot1 + ' %'
        if(tir_tot1 <= 90 || tir_tot1 >= 110){
            //green
            document.getElementById("tir_tot1").className = "table-success";
            document.getElementById("tir_tot2").className = "table-success";

        }else if (tir_tot1 < 105 && tir_tot1 > 95){
            //red
            document.getElementById("tir_tot1").className = "table-danger";
            document.getElementById("tir_tot2").className = "table-danger";

        }else {
            //yellow
            document.getElementById("tir_tot1").className = "table-warning";
            document.getElementById("tir_tot2").className = "table-warning";

        } */
/* 
        //+ De Tirs
        let de_tirs1 = (data[selected1].shotsPerGame / tirs1).toFixed(3)
        document.getElementById('de_tirs1').innerHTML = de_tirs1
        let de_tirs2 = (data[selected2].shotsPerGame / tirs2).toFixed(3)
        document.getElementById('de_tirs2').innerHTML = de_tirs2

        let diff1 = ((data[selected1].shotsPerGame / tirs1).toFixed(2) - (data[selected2].shotsPerGame / tirs2)).toFixed(3)
        if(Math.abs(diff1) >= 0.08){
            //green
            document.getElementById("de_tirs1").className = "table-success";
        }else if (Math.abs(diff1) < 0.08 && Math.abs(diff1) >= 0.05){
            //red
            document.getElementById("de_tirs1").className = "table-warning";
        }else {
            //yellow
            document.getElementById("de_tirs1").className = "table-danger";
        }
        let diff2 = ((data[selected2].shotsPerGame / tirs2).toFixed(2)- (data[selected1].shotsPerGame / tirs1)).toFixed(3)
        if(Math.abs(diff2) >= 0.08){
            //green
            document.getElementById("de_tirs2").className = "table-success";
        }else if (Math.abs(diff2) < 0.08 && Math.abs(diff2) >= 0.05){
            //yellow
            document.getElementById("de_tirs2").className = "table-warning";
        }else {
            //red
            document.getElementById("de_tirs2").className = "table-danger";
        } */

        // tirs prévus, moyenne de tirs + potentiel vs cette equipe
        let nb_tirs1 = ((data[selected1].shotsPerGame * tirs1)/100)
        let nb_tirs2 = ((data[selected2].shotsPerGame * tirs2)/100)
        document.getElementById('de_tirs1').innerHTML = nb_tirs1.toFixed(2)
        document.getElementById('de_tirs2').innerHTML = nb_tirs2.toFixed(2)
        if(Number(nb_tirs1) > Number(nb_tirs2)){
            //green
            document.getElementById("de_tirs1").className = "table-success";
        }else {
            document.getElementById("de_tirs2").className = "table-success";
        }

        // Betty Tirs But (tirs normalisés), moyenne entre tirs prévus (nb_tirs) et shotsPerGame/pot_tirs
        let betty1 = ((nb_tirs1 + (data[selected1].shotsPerGame / tirs1 *100)) /2)
        document.getElementById('betty1').innerHTML = betty1.toFixed(2) + ' Tirs'
        let betty2 = ((nb_tirs2 + (data[selected2].shotsPerGame / tirs2 *100)) /2)
        document.getElementById('betty2').innerHTML = betty2.toFixed(2) + ' Tirs'
        if(Number(betty1) > Number(betty2)){
            //1 WIN
            document.getElementById("betty1").className = "table-success";
        }else{
            //2 WIN
            document.getElementById("betty2").className = "table-success";
        }

        // Nb buts prévus, tirs normalisé / ratio shots:goals de l'equipe
        let nb_buts1 = (betty1 / (data[selected1].shotsPerGame/data[selected1].goalsPerGame)).toFixed(2)
        document.getElementById('nb_buts1').innerHTML = (nb_tirs1 / (data[selected1].shotsPerGame/data[selected1].goalsPerGame)).toFixed(2) + ' / ' + nb_buts1 + ' Buts'
        let nb_buts2 = (betty2 / (data[selected2].shotsPerGame/data[selected2].goalsPerGame)).toFixed(2)
        document.getElementById('nb_buts2').innerHTML = (nb_tirs2 / (data[selected2].shotsPerGame/data[selected2].goalsPerGame)).toFixed(2) + ' / ' + nb_buts2 + ' Buts'
        if(Number(nb_buts1) > Number(nb_buts2)){
            //1 WIN
            document.getElementById("name1").className = "table-success";
            document.getElementById("nb_buts1").className = "table-success";
            document.getElementById("name2").className = "table-danger";
        }else{
            //2 WIN
            document.getElementById("name2").className = "table-success";
            document.getElementById("nb_buts2").className = "table-success";
            document.getElementById("name1").className = "table-danger";
        }
    }
}
main()
